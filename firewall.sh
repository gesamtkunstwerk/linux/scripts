#!/usr/bin/env bash
#
###############################################################################
#                                 DECLARATIONS
###############################################################################
#
NAME="FIREWALL WITH IPTABLES"
VERSION="v1.2"
#
###############################################################################
#                                    COLORS
###############################################################################
#
COL_N="\e[1;0m"
COL_r="\e[0;31m"
COL_R="\e[1;31m"
COL_g="\e[0;32m"
COL_G="\e[1;32m"
COL_y="\e[0;33m"
COL_Y="\e[1;33m"
COL_C="\e[1;36m"
#
###############################################################################
#                                     PATH
###############################################################################
#
PATH_SCR=$(pwd)
#
###############################################################################
#                                  FUNCTIONS
###############################################################################
#
#	SCRIPTS - IPTABLES
#
function iptables_hostapd()
{
	iptables -I INPUT 1 -i ${LAN} -j ACCEPT
	iptables -I INPUT 1 -i lo -j ACCEPT
	iptables -A INPUT -p UDP --dport bootps ! -i ${LAN} -j REJECT
	iptables -A INPUT -p UDP --dport domain ! -i ${LAN} -j REJECT
	iptables -A INPUT -p TCP ! -i ${LAN} -d 0/0 --dport 0:1023 -j DROP
	iptables -A INPUT -p UDP ! -i ${LAN} -d 0/0 --dport 0:1023 -j DROP
	iptables -I FORWARD -i ${LAN} -d 192.168.0.0/255.255.0.0 -j DROP
	iptables -A FORWARD -i ${LAN} -s 192.168.0.0/255.255.0.0 -j ACCEPT
	iptables -A FORWARD -i ${WAN} -d 192.168.0.0/255.255.0.0 -j ACCEPT
	iptables -t nat -A POSTROUTING -o ${WAN} -j MASQUERADE
}

function iptables_new()
{
	iptables -F
	iptables -X
	iptables -Z
	iptables -P INPUT	DROP
	iptables -P FORWARD	DROP
	iptables -P OUTPUT	ACCEPT
	iptables -A INPUT -m conntrack --ctstate ESTABLISHED,RELATED -j ACCEPT
	iptables -A INPUT -i lo -j ACCEPT
	iptables -A INPUT -p icmp --icmp-type 3 -j ACCEPT
	iptables -A INPUT -p icmp --icmp-type 11 -j ACCEPT
	iptables -A INPUT -p icmp --icmp-type 12 -j ACCEPT
	iptables -A INPUT -p tcp --syn --dport 113 -j REJECT \
		--reject-with tcp-reset

	iptables -t nat -A OUTPUT -p TCP --dport 53 -j DNAT \
		--to-destination 127.0.0.1:9053
	iptables -t nat -A OUTPUT -p UDP --dport 53 -j DNAT \
		--to-destination 127.0.0.1:9053
	iptables -t nat -F
	iptables -t nat -X

	iptables -A INPUT -i lan0 -p tcp -s 192.168.1.110 --dport 53 -j ACCEPT
	iptables -A INPUT -i lan0 -p udp -s 192.168.1.110 --dport 53 -j ACCEPT

	iptables -A INPUT -s 192.168.1.110/32 -i lan0 -p tcp -m tcp \
		--dport 1500 -j ACCEPT

	ip6tables -F
	ip6tables -X
	ip6tables -Z
	ip6tables -P INPUT DROP
	ip6tables -P FORWARD DROP
	ip6tables -P OUTPUT DROP
#	ip6tables -A INPUT -m conntrack --ctstate RELATED,ESTABLISHED -j ACCEPT
#	ip6tables -A INPUT -i lo -j ACCEPT
#	ip6tables -A INPUT -m conntrack --ctstate INVALID -j DROP
#	ip6tables -A INPUT -p ipv6-icmp -j ACCEPT
#	ip6tables -A INPUT -p udp -m conntrack --ctstate NEW -j REJECT \
#		--reject-with icmp6-port-unreachable
#	ip6tables -A INPUT -p tcp -m tcp --tcp-flags FIN,SYN,RST,ACK SYN \
#		-m conntrack --ctstate NEW -j	REJECT --reject-with tcp-reset
}

function iptables_vpn()
{
	iptables -t nat -A POSTROUTING -s ${VPN_IP} -o ${LAN} -j MASQUERADE
	iptables -A INPUT	-i tun+ -j ACCEPT
	iptables -A FORWARD	-i tun+ -j ACCEPT
	iptables -A INPUT	-i tap+ -j ACCEPT
	iptables -A FORWARD	-i tap+ -j ACCEPT
	iptables -A INPUT -i ${LAN} -m state --state NEW -p udp \
		--dport ${VPN_PORT} -j ACCEPT
	iptables -A FORWARD	-i tun+		-o ${LAN}	-m state \
		--state RELATED,ESTABLISHED -j ACCEPT
	iptables -A FORWARD	-i ${LAN}	-o tun+		-m state \
		--state RELATED,ESTABLISHED -j ACCEPT
	iptables -A FORWARD	-i tap+		-o ${LAN}	-m state \
		--state RELATED,ESTABLISHED -j ACCEPT
	iptables -A FORWARD	-i ${LAN}	-o tap+		-m state \
		--state RELATED,ESTABLISHED -j ACCEPT
}
#
#	FUNCTION - PRE CONFIGURATION
#
function fun_pre_vpn()
{
	echo -e "${COL_C}[P] Preconfiguration - VPN server\n"

	echo -e -n "${COL_y}Write the interface: ${COL_N}"
	read LAN
	echo -e ""

	echo -e -n "${COL_y}Write the IP family: ${COL_N}"
	read VPN_IP
	echo -e ""

	echo -e -n "${COL_y}Write the VPN port: ${COL_N}"
	read VPN_PORT
	echo -e ""

	echo -e "${COL_R}[!] Check the VPN configuration: LAN=<${LAN}>" \
		"IP:<${VPN_IP}>:<${VPN_PORT}>\n"
	echo -e -n "${COL_y}The data is right <y,n>: ${COL_N}"
	read -n 1 SEL_CONF
	echo -e "\n"

	case ${SEL_CONF} in
	"y"	)
		echo -e "${COL_G}[*] Configuration of firewall (iptables)" \
			"for VPN server\n${COL_N}"
		echo -e "${COL_R}[!] Requires superuser access\n${COL_N}"
		export LAN
		export VPN_IP
		export VPN_PORT
		sudo -E bash -c "$(declare -f conf_vpn);conf_vpn"
		;;
	*)
		return 0
		;;
	esac
}

function fun_pre_hostapd()
{
	echo -e "${COL_C}[P] Preconfiguration - Hostapd server\n"

	echo -e -n "${COL_y}Write the LAN: ${COL_N}"
	read LAN
	echo -e ""

	echo -e -n "${COL_y}Write the WAN: ${COL_N}"
	read WAN
	echo -e ""

	echo -e "${COL_R}[!] Check the interface: LAN=<${LAN}>" \
		"WAN:<${WAN}>\n"
	echo -e -n "${COL_y}The data is right <y,n>: ${COL_N}"
	read -n 1 SEL_CONF
	echo -e "\n"

	case ${SEL_CONF} in
	"y"	)
		echo -e "${COL_G}[*] Configuration of firewall (iptables)" \
			"for Hostapd\n${COL_NL}"
		echo -e "${COL_R}[!] Requires superuser access\n${COL_N}"
		export LAN
		export WAN
		sudo -E bash -c \
			"$(declare -f iptables_hostapd);iptables_hostapd"
		;;
	*)
		return 0
		;;
	esac
}
#
#	FUNCTION - MAIN OPTIONS
#
function opt_new()
{
	echo -e "${COL_C}[P] New configuration of firewall\n"
	echo -e "${COL_G}[*] Configuration of firewall" \
		"(iptables)\n${COL_N}"
	echo -e "${COL_R}[!] Requires superuser access\n${COL_N}"
	sudo bash -c "$(declare -f iptables_new);iptables_new"
}

function opt_open()
{
	echo -e "${COL_C}[P] Open one port\n"
	echo -e "${COL_Y}[?] Select the port and ip\n"

	echo -e -n "${COL_y}Write the protocol <tcp,udp>: ${COL_N}"
	read -n 3 SEL_PRO
	echo -e "\n"

	[ "${SEL_PRO}" != "tcp" ] && [ "${SEL_PRO}" != "udp" ] &&
		echo -e "${COL_R}[!] Command not found\n${COL_N}" &&
		return -1

	echo -e -n "${COL_y}Write the interface: ${COL_N}"
	read LAN
	echo -e ""

	echo -e -n "${COL_y}Write the port: ${COL_N}"
	read -n 4 SEL_PORT
	echo -e "\n"

	echo -e -n "${COL_y}Write the ip family: ${COL_N}"
	read SEL_IP
	echo -e ""

	echo -e "${COL_R}[!] Open port: ${SEL_IP}:${SEL_PORT} <${SEL_PRO}>\n"
	echo -e -n "${COL_y}The data is right <y,n>: ${COL_N}"
	read -n 1 SEL_CONF
	echo -e "\n"

	case ${SEL_CONF} in
	"y"	)
		echo -e "${COL_G}[*] Open the port (iptables)\n${COL_N}"
		echo -e "${COL_R}[!] Requires superuser access\n${COL_N}"
		sudo iptables -A INPUT -i ${LAN} -p ${SEL_PRO} -s ${SEL_IP} \
			--dport ${SEL_PORT} -j ACCEPT
		;;
	*)
		return 0
		;;
	esac
}

function opt_pre()
{
	echo -e "${COL_C}[P] Add preconfiguration port for services\n"

	echo -e "${COL_Y}[?] Select the next options:${COL_y}\n"
	echo -e "\t1) VPN server"
	echo -e "\t2) Hostapd server"
	echo -e "\n"

	echo -e -n "Write the option: ${COL_N}"
	read -n 1 OPT
	echo -e "\n"

	case ${OPT} in
	"1"	)
		fun_pre_vpn
		;;
	"2"	)
		fun_pre_hostapd
		;;
	*)
		echo -e "${COL_R}[!] Command unknown\n"
		;;
	esac
}
#
###############################################################################
#                                     MAIN
###############################################################################
#
clear
echo -e "\n\t\t${COL_R}SCRIPT - ${NAME} - ${VERSION}"
echo -e "\nby Hans von Hohenstaufen${COL_N}\n"
#
#	SELECT OPTION
#
echo -e "${COL_Y}[?] Select the next options:${COL_y}\n"
echo -e "\t1) New configuration of firewall (iptables)"
echo -e "\t2) Add preconfiguration port for services"
echo -e "\t3) Open one port"
echo -e "\n"

echo -e -n "Write the option: ${COL_N}"
read -n 1 OPT
echo -e "\n"

case ${OPT} in
"1"	)
	opt_new
	;;
"2"	)
	opt_pre
	;;
"3"	)
	opt_open
	;;
*)
	echo -e "${COL_R}[!] Command unknown\n"
	;;
esac
