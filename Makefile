# scripts

default: dist

clean:
	rm -f scripts.tar.xz

dist: clean
	mkdir -p scripts
	cp -R LICENSE README Makefile \
		firewall.sh update_playlist.sh \
		scripts
	tar -cf scripts.tar scripts
	xz -9 -T0 scripts.tar
	rm -rf scripts

.PHONY: clean dist
