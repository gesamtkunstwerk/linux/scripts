#!/usr/bin/env bash
#
###############################################################################
#                                 DECLARATION
###############################################################################
#
NAME="UPDATE CMUS'S PLAYLIST"
VERSION="v1.1"
#
###############################################################################
#                                    COLORS
###############################################################################
#
export COL_NUL="\e[1;0m"
export COL_RED="\e[1;31m"
export COL_GRE="\e[1;32m"
#
###############################################################################
#                                     PATH
###############################################################################
#
export PATH_NOW=$(pwd)
export PATH_CMUS=${HOME}/.config/cmus/playlists
#
###############################################################################
#                                  FUNCTIONS
###############################################################################
#
playlist()
{
	echo -e "${COL_GRE}Create - ${COL_NUL}${1}"
	find ${PATH_NOW}/${1}/ -type f -iname "*.flac" > ${PATH_CMUS}/${1}
	sort ${PATH_CMUS}/${1} -o ${PATH_CMUS}/${1}
}
#
###############################################################################
#                                     MAIN
###############################################################################
#
clear
echo -e "\n\t\t${COL_RED}SCRIPT - ${NAME} - ${VERSION}"
echo -e "\nby Hans von Hohenstaufen${COL_NULL}\n"
#
#	FIND DIRECTORIES, FILES AND SAVE THE PLAYLIST
#
echo -e "${COL_GREEN}[*] Find directories\n${COL_NULL}"
mkdir -p ${PATH_CMUS}
export -f playlist
find . -type d | cut -c3- | sed -E '/^[[:space:]]*$/d' | sort | \
	xargs -I % bash -c "playlist \"%\""

echo ""
